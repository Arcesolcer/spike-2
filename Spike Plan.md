# Spike Plan

## Core 2 - Blueprint Basics

### Context

In Unreal Engine, a lot of work is completed using Blueprints, a form of Visual Programming/Scripting.
We hopefully have some exposure from SGD102.  Either way we need to either revise or learn these points.

### Gap

1. We do not know the capabilities of the Blueprint system
2. We do not know how to use the Blueprint system

### Goals / Deliverables

1. The Spike Report should answer each of the Gap questions
2. In a new Blueprint FPS Unreal Engine project, create a “Launch-pad” actor, which can be placed to cause the player to leap into the air upon stepping on it.
    * A. Play a sound when the character is launched
    * B. Set the Launch Velocity using a variable, which lets you place multiple launch-pads with different velocities in the same level
    * C. Add an Arrow Component, and use its rotation to define the direction to launch the character
3. Create a new level using the default elements available to you from the Unreal Engine (boxes should suffice), which should be a fun little FPS level with many launch-pads that help you access different areas.

### Dates

Planned start date: week 1
Deadline: week 2

### Planning Notes

1. Skim the [Unreal Engine Blueprints Visual Scripting Documentation](https://docs.unrealengine.com/latest/INT/Engine/Blueprints/index.html "Scripting Documentation")
    * A. The [Quick Start Guide](https://docs.unrealengine.com/latest/INT/Engine/Blueprints/QuickStart/index.html "Example Guide") tutorial will guide you through most of this spike.
    * B. The [Blueprint Class Creation](https://docs.unrealengine.com/latest/INT/Gameplay/ClassCreation/index.html "Blueprint Class Guide") Guide may be helpful for a more general understanding.
2. In the standard FPS level:
    *A. Build a Launchpad in the level editor
    *B. Convert it to a Blueprint
    *C. Add a trigger collider, and prepare the OnComponentBeginOverlap event
    *D. Make sure the Other Actor is the Player Pawn
    *E. Call the Launch Character function on the Player (casting the Other Actor to a Character)
3. Perhaps this could help?
    *A. [UE4 Blueprints](https://www.raywenderlich.com/156038/unreal-engine-4-blueprints-tutorial "UE4 Blueprint Tutorial")
    *B. Can you find any other useful learning resources?
4. Then create your own level!
