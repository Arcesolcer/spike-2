# Spike Report

## Core 2 - Blueprint Basics

### Introduction

This spike report is designed to work on blueprints, a main component of UE4's Scripting and programming. By rerunning though them it helps revise or relearn these skills in UE4's blueprint programming.

### Goals

1. The Spike Report should answer each of the Gap questions

2. In a new Blueprint FPS Unreal Engine project, create a “Launch-pad” actor, which can be placed to cause the player to leap into the air upon stepping on it.
   * A. Play a sound when the character is launched
   * B. Set the Launch Velocity using a variable, which lets you place multiple launch-pads with different velocities in the same level
   * C. Add an Arrow Component, and use its rotation to define the direction to launch the character
  
3. Create a new level using the default elements available to you from the Unreal Engine (boxes should suffice), which should be a fun little FPS level with many launch-pads that help you access different areas.

### Personnel

* Primary - Geordie

### Technologies, Tools, and Resources used

* Unreal Engine
* SourceTree
* Visual Studio Code
* [Main exercise](https://docs.unrealengine.com/en-US/Engine/Blueprints/QuickStart/index.html "Blueprints Quick Start Guide"), really useful for the majority of this spike and the goals set out to complete, excellent for learning the basics of blueprints
* [Guide for Blueprints](https://www.raywenderlich.com/156038/unreal-engine-4-blueprints-tutorial "Unreal Engine 4 Blueprints Tutorial"), Good for getting familiar with Unreal Engine and general use blueprints
* [Guide to Audio Cues](https://docs.unrealengine.com/en-US/Engine/Audio/SoundCues/Editor/index.html "Sound Cue Editor"), Excellent for setting up and explaining how to implement audio with collision objects
* [Audio Cue Video](https://www.youtube.com/watch?v=qiAx1dCs1VI "Unreal Engine 4 - Sound Cues Tutorial by Markom3D") by Markom3D to set up an example, helps to follow along and useful for learning about the echo effect that may happen if the collision box is too big and hitting other objects
* [Sound-Board](https://freesound.org/browse/ "Freesound"), Good for sounds that fit specific needs
* [Video](https://www.youtube.com/watch?v=Q0OrlqcAOdM "Launch Character | Tutorial | Unreal Engine 4 Blueprints by Realtime 3D NowYoshi") by Realtime 3D NowYoshi, Helps setting up the launchpad, and make it a public variable

### Tasks Undertaken

1. Added Launchpad and followed this [Main exercise](https://docs.unrealengine.com/en-US/Engine/Blueprints/QuickStart/index.html "Blueprints Quick Start Guide").
2. Added a Sound cue after grabbing a bounce sound effect from [Sound-Board](https://freesound.org/browse/ "Freesound"), and followed this [Video](https://www.youtube.com/watch?v=qiAx1dCs1VI "Unreal Engine 4 - Sound Cues Tutorial by Markom3D") by Markom3D to make sure the sound was implemented properly.
3. Fixed some blueprint components with [Video](https://www.youtube.com/watch?v=Q0OrlqcAOdM "Launch Character | Tutorial | Unreal Engine 4 Blueprints by Realtime 3D NowYoshi") going over the variable setup.
4. Updating the blueprint also required to include an arrow. So the arrow was set up in the blueprint as a component and used 'GetForwardVector' from the arrow and multiplied it with the variable created in the last task undertaken for it to point the arrow and propell the player in the direction it was facing. (This step I asked some developers for help via discord)
5. Then set up a level adding some geometry objects for extra space and duplicating the map level.

### What we found out

How to work blueprints, and set up a launchpad with a directional arrow to display the direction of the launchpad. Easy ways to set up variables in the blueprint by right clicking the node and selecting create variable.

How to add Audio sounds and add sound cues, which is much easier to set up then expected, just need to have an audio file to put in and a audio cue is easy to set up from there.

### [Optional] Open Issues/Risks

Null

### [Optional] Recommendations

It would be a good idea to be more generally aware of how to use Unreal Engine and it's component properties, as it helps when setting up a new level.

The arrow component to point the launchpad in the direction it launches the player model is a little tricky if you are unfamiliar with the unreal engine and game coding in general, so a reading into how to set up components and implement them effectively in blueprints is recommended before starting this spike. (I'm Guilty for just following the guide and not properly going through the components of the blueprint)
